﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class CustomSocketClient
    {
        private ClientWebSocket client;

        private string _strAdressEndpoint { get; }
        public string _strNotifyMessage { get; set; }

        public CustomSocketClient(string strAdressEndpoint)
        {
            _strAdressEndpoint = strAdressEndpoint;
            Initialize();
        }

        private void Initialize()
        {
            client = new ClientWebSocket();
            _strNotifyMessage = "notify message";
        }

        public async Task Start()
        {
            await OpenConnection();
        }

        private async Task OpenConnection()
        {
            if (client.State != WebSocketState.Open)
            {
                await client.ConnectAsync(new Uri(_strAdressEndpoint), CancellationToken.None); //ToDo built in CancellationToken
            }
        }

        public async Task<string> Receive()
        {
            byte[] buffer = new byte[1024];
            WebSocketReceiveResult result = await client.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);//ToDo built in CancellationToken

            if (result.MessageType == WebSocketMessageType.Close)
            {
                return "abort";
            }

            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(buffer, 0, result.Count);
                while (!result.EndOfMessage)
                {
                    result = await client.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);//ToDo built in CancellationToken
                    stream.Write(buffer, 0, result.Count);
                }

                stream.Seek(0, SeekOrigin.Begin);
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    string message = reader.ReadToEnd();
                    return message;
                }
            }
        }

        public async Task Send(string message)
        {
            if (client.State == WebSocketState.Open)
            {
                byte[] byteContentBuffer = Encoding.UTF8.GetBytes(message);
                await client.SendAsync(new ArraySegment<byte>(byteContentBuffer), WebSocketMessageType.Text, true, CancellationToken.None); //ToDo built in CancellationToken
            }
        }
    }
}
