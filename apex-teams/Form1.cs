﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;
using Newtonsoft.Json;
using System.IO;
using Tesseract;
using System.Threading.Tasks;
using Gma.System.MouseKeyHook;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        static int activeWeaponIndex = 0;
        static int numberBitmapNumberCount = 10;
        static int numberBitmapPointCount = 10;
        static bool debugMode = true;
        static Point[,] bitmapPoints = new Point[11, 10];
        static Dictionary<string, Bitmap> weaponBgs = new Dictionary<string, Bitmap>();
        static Dictionary<string, Bitmap> ammoBgs = new Dictionary<string, Bitmap>();
        static Dictionary<string, Weapon> weapons = new Dictionary<string, Weapon>();
        static Dictionary<string, string> weaponNames = new Dictionary<string, string>();
        static Dictionary<string, string> activeColors = new Dictionary<string, string>();
        static Dictionary<string, string> inactiveColors = new Dictionary<string, string>();
        static Dictionary<string, string[]> numberColors = new Dictionary<string, string[]>();
        System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer keyStrokeTimer = new System.Windows.Forms.Timer();
        UserData userData;
        Weapon weapon1 = new Weapon("unknown", "-");
        Weapon weapon2 = new Weapon("unknown", "-");
        string ammo1 = "???";
        string ammo2 = "???";
        User user;
        User[] users;
        static TesseractEngine ocrengine;
        string username = "unset";
        FloatingOSDWindow floatingOsdWindow;
        string ammoImg1;
        string ammoImg2;
        string ammoImg1Color;
        string ammoImg2Color;

        private void toggleDebugMode()
        {
            floatingOsdWindow.debugMode = !floatingOsdWindow.debugMode;
            floatingOsdWindow.TimerEventProcessor(null, null);
        }
        private void TimerEventProcessor(Object myObject,
                                            EventArgs myEventArgs)
        {
            Unsubscribe();
            if (userData.checkNumbers)
            {
                Close();
            }
            Console.WriteLine("__________________________________________________");
            Rectangle weapon1Rect = new Rectangle(userData.weapon1ReadBox.x, userData.weapon1ReadBox.y, userData.weapon1ReadBox.width, userData.weapon1ReadBox.height);
            Bitmap weapon1Bmp = new Bitmap(weapon1Rect.Width, weapon1Rect.Height);
            Graphics weapon1Graphics = Graphics.FromImage(weapon1Bmp);
            weapon1Graphics.CopyFromScreen(weapon1Rect.Left, weapon1Rect.Top, 0, 0, weapon1Bmp.Size, CopyPixelOperation.SourceCopy);
            weapon1Bmp.Save("res/Weapon1.png", System.Drawing.Imaging.ImageFormat.Png);
            var c1p = weapon1Bmp.GetPixel(3, 3);
            var c1Hex = HexConverter(c1p).ToLower();
            var Text1 = ReadWeaponName(1);
            if (userData.debugMode)
            {
                var displayString = Text1 == "" ? "Read Failed" : Text1;
                floatingOsdWindow.Weapon1ReadText = displayString;
                floatingOsdWindow.Weapon1NameInDic = weaponNames.ContainsKey(Text1);
            }
            if (weaponNames.ContainsKey(Text1))
            {
                var resolvedText1 = weaponNames[Text1];

                var newWeapon1 = weapons[resolvedText1];
                if (weapon1.name != newWeapon1.name)
                {
                    if (weapon1.ammoType != newWeapon1.ammoType) ammo1 = "???";
                    weapon1 = newWeapon1;

                }
            }
            Rectangle weapon2Rect = new Rectangle(userData.weapon2ReadBox.x, userData.weapon2ReadBox.y, userData.weapon2ReadBox.width, userData.weapon2ReadBox.height);
            Bitmap weapon2Bmp = new Bitmap(weapon2Rect.Width, weapon2Rect.Height);
            Graphics weapon2Graphics = Graphics.FromImage(weapon2Bmp);
            weapon2Graphics.CopyFromScreen(weapon2Rect.Left, weapon2Rect.Top, 0, 0, weapon2Bmp.Size, CopyPixelOperation.SourceCopy);
            weapon2Bmp.Save("res/Weapon2.png", System.Drawing.Imaging.ImageFormat.Png);
            var c2p = weapon2Bmp.GetPixel(3, 3);
            var c2Hex = HexConverter(c2p).ToLower();
            var Text2 = ReadWeaponName(2);
            if (userData.debugMode)
            {
                var displayString = Text2 == "" ? "Read Failed" : Text2;
                floatingOsdWindow.Weapon2ReadText = displayString;
                floatingOsdWindow.Weapon2NameInDic = weaponNames.ContainsKey(Text2);
            }
            if (weaponNames.ContainsKey(Text2))
            {
                var resolvedText2 = weaponNames[Text2];

                var newWeapon2 = weapons[resolvedText2];
                if (weapon2.name != newWeapon2.name)
                {
                    if (weapon2.ammoType != newWeapon2.ammoType) ammo2 = "???";
                    weapon2 = newWeapon2;
                }
            }

            string[] numberColor = { };
            if (activeColors.ContainsKey(c1Hex) || activeColors.ContainsKey(c2Hex))
            {
                if (activeColors.ContainsKey(c1Hex))
                {
                    if (!inactiveColors.ContainsKey(c2Hex))
                    {
                        weapon2 = new Weapon("???", "???");
                        ammo2 = "";
                    }
                    activeWeaponIndex = 1;
                    var ammoType = activeColors[c1Hex];
                    numberColor = numberColors[ammoType];
                    ammo1 = "";
                }
                if (activeColors.ContainsKey(c2Hex))
                {
                    if (!inactiveColors.ContainsKey(c1Hex))
                    {
                        ammo1 = "";
                        weapon1 = new Weapon("???", "???");
                    }
                    activeWeaponIndex = 2;
                    var ammoType = activeColors[c2Hex];
                    numberColor = numberColors[ammoType];
                    ammo2 = "";
                }
                if (userData.debugMode)
                {
                    floatingOsdWindow.ammo1ReadText = "-";
                    floatingOsdWindow.ammo2ReadText = "-";
                    floatingOsdWindow.ammo3ReadText = "-";
                }
                string lastAmmo = "000";
                if (activeWeaponIndex == 1) lastAmmo = ammo1;
                if (activeWeaponIndex == 2) lastAmmo = ammo2;
                for (var i = 0; i < 3; i++)
                {
                    Rectangle rect = new Rectangle(userData.ammoReadBox.x + (userData.ammoReadBox.width + userData.ammoSpacing) * i, userData.ammoReadBox.y, userData.ammoReadBox.width, userData.ammoReadBox.height);
                    Bitmap bmp = new Bitmap(rect.Width, rect.Height);
                    Graphics g = Graphics.FromImage(bmp);
                    g.CopyFromScreen(rect.Left, rect.Top, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);

                    if (i == 1 && debugMode)
                    {
                        System.IO.MemoryStream ms = new MemoryStream();
                        bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] byteImage = ms.ToArray();
                        ammoImg1 = Convert.ToBase64String(byteImage);
                        ammoImg1Color = numberColor[0];
                    }
                    if (i == 2 && debugMode)
                    {
                        System.IO.MemoryStream ms = new MemoryStream();
                        bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] byteImage = ms.ToArray();
                        ammoImg2 = Convert.ToBase64String(byteImage);
                        ammoImg2Color = numberColor[0];
                    }
                    var number = "";
                    for (var ii = 0; ii < numberColor.Length; ii++)
                    {
                        var s = getValueFromAmmo(bmp, numberColor[ii]);
                        if (s.Length > 0)
                        {
                            number = s;
                            if (userData.debugMode)
                            {
                                if (i == 0) floatingOsdWindow.ammo1ReadText = number;
                                if (i == 1) floatingOsdWindow.ammo2ReadText = number;
                                if (i == 2) floatingOsdWindow.ammo3ReadText = number;
                            }
                        }
                    }


                    if (number.Length > 1)
                    {
                        Console.WriteLine("Warning! Read ammo position " + i + " and found the following values: " + number + ". Defaulting to last Ammo");
                        number = lastAmmo[i]+"";
                    }

                    if (number.Length == 0) number = "0";
                    if (activeWeaponIndex == 1) ammo1 += number[0];
                    if (activeWeaponIndex == 2) ammo2 += number[0];
                }
                if (activeWeaponIndex == 1 && (ammo1 == "" || ammo1 == "-1-1-1")) ammo1 = "0";
                if (activeWeaponIndex == 2 && (ammo2 == "" || ammo2 == "-1-1-1")) ammo2 = "0";
            }
            else
            {
                Console.WriteLine("No Active Weapon detected");
            }
            if (weapon1.ammoType == weapon2.ammoType && weapon1.ammoType != "special" && weapon2.ammoType != "special")
            {
                if (activeWeaponIndex == 1) ammo2 = "" + ammo1;
                if (activeWeaponIndex == 2) ammo1 = "" + ammo2;
            }
            if (userData.debugMode)
            {
                setAmmo11(weapon1.name + ", " + ammo1 + " Bullets");
                setAmmo12(weapon2.name + ", " + ammo2 + " Bullets");
            }
            new Action(async () =>
            {
                await client.Start();
                user.weapon1 = weapon1.name;
                user.weapon1ammo = ammo1;
                user.weapon1type = weapon1.ammoType == null ? "" : weapon1.ammoType;
                user.weapon2 = weapon2.name;
                user.weapon2ammo = ammo2;
                user.weapon2type = weapon2.ammoType == null ? "" : weapon2.ammoType;
                user.ammoImg1 = ammoImg1;
                user.ammoImg2 = ammoImg2;
                user.ammoImg1Color = ammoImg1Color;
                user.ammoImg2Color = ammoImg2Color;
                var json = JsonConvert.SerializeObject(user);
                await client.Send(json);
            })();
            floatingOsdWindow.Invalidate();
            SubscribeKeyHook();
        }
        ColorConverter colorConverter = new ColorConverter();
        int treshhold = 40;
        private bool isSimilar(String c, String c2)
        {
            Color cc = (Color)colorConverter.ConvertFromString(c);
            Color cc2 = (Color)colorConverter.ConvertFromString(c2);
            int r = Math.Abs(cc.R - cc2.R);
            int g = Math.Abs(cc.G - cc2.G);
            int b = Math.Abs(cc.B - cc2.B);
            return r + g + b < treshhold;
        }
        private String getValueFromAmmo(Bitmap bmp, string numberColor)
        {
            var number = "";
            for (var nr = 0; nr < numberBitmapNumberCount; nr++)
            {
                var matchingPixelCount = 0;
                for (var px = 0; px < numberBitmapPointCount; px++)
                {
                    var point = bitmapPoints[nr, px];
                    var invert = false;
                    if (point.X < 0) invert = true;
                    var pixel = bmp.GetPixel(invert ? -point.X : point.X, invert ? -point.Y : point.Y);

                    var color = HexConverter(pixel).ToLower();
                    //    if (!invert && color == numberColor || invert && color != numberColor)
                    if (!invert &&isSimilar(color,numberColor) || invert && !isSimilar(color, numberColor))
                    {
                        matchingPixelCount++;
                    }
                }
                if (matchingPixelCount == numberBitmapPointCount)
                {
                    number += "" + nr;
                }
            }
            return number;
        }
        private void renderUserInformation()
        {
            floatingOsdWindow.resetPlayerRender();
            foreach (User user in users)
            {
                if (user.name == userData.player1Name)
                {
                    floatingOsdWindow.player1weapon1Draw.weaponBg = weaponBgs[user.weapon1type];
                    floatingOsdWindow.player1weapon1Draw.weaponName = user.weapon1;
                    floatingOsdWindow.player1ammo1Draw.ammoBg = ammoBgs[user.weapon1type];
                    floatingOsdWindow.player1ammo1Draw.count = user.weapon1ammo;
                    floatingOsdWindow.player1weapon2Draw.weaponBg = weaponBgs[user.weapon2type];
                    floatingOsdWindow.player1weapon2Draw.weaponName = user.weapon2;
                    floatingOsdWindow.player1ammo2Draw.ammoBg = ammoBgs[user.weapon2type];
                    floatingOsdWindow.player1ammo2Draw.count = user.weapon2ammo;
                }
                if (user.name == userData.player2Name)
                {
                    floatingOsdWindow.player2weapon1Draw.weaponBg = weaponBgs[user.weapon1type];
                    floatingOsdWindow.player2weapon1Draw.weaponName = user.weapon1;
                    floatingOsdWindow.player2ammo1Draw.ammoBg = ammoBgs[user.weapon1type];
                    floatingOsdWindow.player2ammo1Draw.count = user.weapon1ammo;
                    floatingOsdWindow.player2weapon2Draw.weaponBg = weaponBgs[user.weapon2type];
                    floatingOsdWindow.player2weapon2Draw.weaponName = user.weapon2;
                    floatingOsdWindow.player2ammo2Draw.ammoBg = ammoBgs[user.weapon2type];
                    floatingOsdWindow.player2ammo2Draw.count = user.weapon2ammo;
                }

            }

        }
        CustomSocketClient client;
        public async void awaitData()
        {
            await client.Start();
            string message = await client.Receive();
            users = JsonConvert.DeserializeObject<User[]>(message);
            renderUserInformation();
            awaitData();
        }
        public Form1()
        {
            SubscribeKeyHook();
            InitializeComponent();
            ocrengine = new TesseractEngine(@".\tessdata", "eng", EngineMode.Default);
            using (StreamReader r = new StreamReader("userData.json"))
            {
                string json = r.ReadToEnd();
                userData = JsonConvert.DeserializeObject<UserData>(json);
                username = userData.name;
                debugMode = userData.debugMode;
                createObjects();

                if (userData.checkNumbers)
                {
                    string[] files = Directory.GetFiles("ammoImages");
                    for (var i = 0; i < files.Length; i++)
                    {
                        Bitmap b = new Bitmap(files[i]);
                        if(files[i]== "ammoImages\\513-#b47b44.png")
                        {
                            var x = 0;
                        }
                        string colorWithImg = files[i].Split('-')[1].Split('.')[0];
                        var ammoType = "";
                        foreach (string c in numberColors.Keys)
                        {
                            foreach(string cc in numberColors[c])
                            {
                                if(cc==colorWithImg)
                                {
                                    ammoType = c;
                                }
                            }
                        }
                        string[] colors = numberColors[ammoType];
                        var val = "";
                        for (var j = 0; j < colors.Length; j++)
                        {
                            val += getValueFromAmmo(b, colors[j]);
                            if (val.Length != 0)
                            {
                                j = colors.Length;
                            }
                        }
                        if (val.Length == 0)
                        {
                            b.Dispose();
                            File.Move(files[i], "unknown" + files[i]);
                        }
                        val = "";
                    }
                }

                floatingOsdWindow = new FloatingOSDWindow(0, 0, userData.screenBox.width, userData.screenBox.height, userData.debugMode);
                floatingOsdWindow.player1Name = userData.player1Name;
                floatingOsdWindow.player2Name = userData.player2Name;
                floatingOsdWindow.player1weapon1Draw.posX = userData.player1Weapon1DrawLocation.x;
                floatingOsdWindow.player1weapon1Draw.posY = userData.player1Weapon1DrawLocation.y;
                floatingOsdWindow.player1weapon2Draw.posX = userData.player1Weapon2DrawLocation.x;
                floatingOsdWindow.player1weapon2Draw.posY = userData.player1Weapon2DrawLocation.y;

                floatingOsdWindow.player2weapon1Draw.posX = userData.player2Weapon1DrawLocation.x;
                floatingOsdWindow.player2weapon1Draw.posY = userData.player2Weapon1DrawLocation.y;
                floatingOsdWindow.player2weapon2Draw.posX = userData.player2Weapon2DrawLocation.x;
                floatingOsdWindow.player2weapon2Draw.posY = userData.player2Weapon2DrawLocation.y;

                floatingOsdWindow.player1ammo1Draw.posX = userData.player1Ammo1DrawLocation.x;
                floatingOsdWindow.player1ammo1Draw.posY = userData.player1Ammo1DrawLocation.y;
                floatingOsdWindow.player1ammo2Draw.posX = userData.player1Ammo2DrawLocation.x;
                floatingOsdWindow.player1ammo2Draw.posY = userData.player1Ammo2DrawLocation.y;

                floatingOsdWindow.player2ammo1Draw.posX = userData.player2Ammo1DrawLocation.x;
                floatingOsdWindow.player2ammo1Draw.posY = userData.player2Ammo1DrawLocation.y;
                floatingOsdWindow.player2ammo2Draw.posX = userData.player2Ammo2DrawLocation.x;
                floatingOsdWindow.player2ammo2Draw.posY = userData.player2Ammo2DrawLocation.y;
                if (debugMode)
                {
                    floatingOsdWindow.CreateBox(userData.weapon1ReadBox.x, userData.weapon1ReadBox.y, userData.weapon1ReadBox.width, userData.weapon1ReadBox.height, "weapon1ReadBox");
                    floatingOsdWindow.CreateBox(userData.weapon2ReadBox.x, userData.weapon2ReadBox.y, userData.weapon2ReadBox.width, userData.weapon2ReadBox.height, "weapon2ReadBox");

                    floatingOsdWindow.CreateBox(userData.player1NameBox.x, userData.player1NameBox.y, userData.player1NameBox.width, userData.player1NameBox.height, "player1NameBox");
                    floatingOsdWindow.CreateBox(userData.player2NameBox.x, userData.player2NameBox.y, userData.player2NameBox.width, userData.player2NameBox.height, "player2NameBox");

                    floatingOsdWindow.CreateBox(userData.ammoReadBox.x, userData.ammoReadBox.y, userData.ammoReadBox.width, userData.ammoReadBox.height, "ammoReadBox1");
                    floatingOsdWindow.CreateBox((userData.ammoReadBox.x + (userData.ammoSpacing + userData.ammoReadBox.width)), userData.ammoReadBox.y, userData.ammoReadBox.width, userData.ammoReadBox.height, "ammoReadBox2");
                    floatingOsdWindow.CreateBox((userData.ammoReadBox.x + (userData.ammoSpacing + userData.ammoReadBox.width) * 2), userData.ammoReadBox.y, userData.ammoReadBox.width, userData.ammoReadBox.height, "ammoReadBox3");
                    if (!debugMode) this.Hide();
                }

            }
            client = new CustomSocketClient("ws://85.214.56.221:9900/");
            //client = new CustomSocketClient("ws://localhost:9900/");
            user = new User(username, "???", "???", "???", "???", "???", "???");

            new Action(async () =>
            {
                await client.Start();
                user.loopBack = userData.loopBack;
                var json = JsonConvert.SerializeObject(user);
                await client.Send(json);
                awaitData();
            })();

            myTimer.Tick += new EventHandler(TimerEventProcessor);
            myTimer.Interval = 3000;
            myTimer.Start();
        }
        public void setAmmo11(string a)
        {
            player1Ammo1.Text = a;
        }
        public void setAmmo12(string a)
        {
            player1Ammo2.Text = a;
        }
        private void player1_Click(object sender, EventArgs e)
        {

        }


        private static String HexConverter(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
        public static String ReadWeaponName(int nr)
        {
            var img = Pix.LoadFromFile(@"res/Weapon" + nr + ".png");
            var res = ocrengine.Process(img);
            var t = res.GetText();
            t = t.Replace("\n", "");
            res.Dispose();
            return t;
        }
        public static void createObjects()
        {
            weaponBgs.Add("light", new Bitmap(@"res/BGlight.png"));
            weaponBgs.Add("heavy", new Bitmap(@"res/BGheavy.png"));
            weaponBgs.Add("sniper", new Bitmap(@"res/BGsniper.png"));
            weaponBgs.Add("special", new Bitmap(@"res/BGspecial.png"));
            weaponBgs.Add("shotgun", new Bitmap(@"res/BGshotgun.png"));
            weaponBgs.Add("energy", new Bitmap(@"res/BGenergy.png"));
            weaponBgs.Add("???", null);

            ammoBgs.Add("light", new Bitmap(@"res/lightAmmo.png"));
            ammoBgs.Add("heavy", new Bitmap(@"res/heavyAmmo.png"));
            ammoBgs.Add("sniper", new Bitmap(@"res/sniperAmmo.png"));
            ammoBgs.Add("special", new Bitmap(@"res/specialAmmo.png"));
            ammoBgs.Add("shotgun", new Bitmap(@"res/shotgunAmmo.png"));
            ammoBgs.Add("energy", new Bitmap(@"res/energyAmmo.png"));
            ammoBgs.Add("???", null);

            numberColors.Add("light", new string[] { "#b47b44", "#b37b44" });
            numberColors.Add("heavy", new string[] { "#549b83" });
            numberColors.Add("energy", new string[] { "#84a03d" });
            numberColors.Add("shotgun", new string[] { "#9b330f" });
            numberColors.Add("sniper", new string[] { "#6e5fce" });
            numberColors.Add("special", new string[] { "#ff0253" });

            bitmapPoints[0, 0] = new Point(2, 2);
            bitmapPoints[0, 1] = new Point(11, 2);
            bitmapPoints[0, 2] = new Point(4, 6);
            bitmapPoints[0, 3] = new Point(9, 12);
            bitmapPoints[0, 4] = new Point(3, 17);

            bitmapPoints[0, 5] = new Point(1, 6);
            bitmapPoints[0, 6] = new Point(8, 10);
            bitmapPoints[0, 7] = new Point(11, 13);
            bitmapPoints[0, 8] = new Point(1, 12);
            bitmapPoints[0, 9] = new Point(12, 6);



            bitmapPoints[1, 0] = new Point(2, 1);
            bitmapPoints[1, 1] = new Point(6, 1);
            bitmapPoints[1, 2] = new Point(6, 5);
            bitmapPoints[1, 3] = new Point(2, 17);
            bitmapPoints[1, 4] = new Point(11, 17);

            bitmapPoints[1, 5] = new Point(7, 2);
            bitmapPoints[1, 6] = new Point(7, 12);
            bitmapPoints[1, 7] = new Point(11, 18);
            bitmapPoints[1, 8] = new Point(4, 18);
            bitmapPoints[1, 9] = new Point(5, 17);



            bitmapPoints[2, 0] = new Point(1, 4);
            bitmapPoints[2, 1] = new Point(3, 1);
            bitmapPoints[2, 2] = new Point(12, 3);
            bitmapPoints[2, 3] = new Point(11, 8);
            bitmapPoints[2, 4] = new Point(1, 18);

            bitmapPoints[2, 5] = new Point(4, 11);
            bitmapPoints[2, 6] = new Point(11, 17);
            bitmapPoints[2, 7] = new Point(6, 18);
            bitmapPoints[2, 8] = new Point(1, 13);
            bitmapPoints[2, 9] = new Point(9, 9);



            bitmapPoints[3, 0] = new Point(1, 4);
            bitmapPoints[3, 1] = new Point(5, 9);
            bitmapPoints[3, 2] = new Point(11, 8);
            bitmapPoints[3, 3] = new Point(12, 14);
            bitmapPoints[3, 4] = new Point(1, 15);

            bitmapPoints[3, 5] = new Point(6, 10);
            bitmapPoints[3, 6] = new Point(13, 13);
            bitmapPoints[3, 7] = new Point(2, 15);
            bitmapPoints[3, 8] = new Point(12, 17);
            bitmapPoints[3, 9] = new Point(-2, -9);



            bitmapPoints[4, 0] = new Point(1, 1);
            bitmapPoints[4, 1] = new Point(12, 1);
            bitmapPoints[4, 2] = new Point(3, 9);
            bitmapPoints[4, 3] = new Point(12, 9);
            bitmapPoints[4, 4] = new Point(12, 18);

            bitmapPoints[4, 5] = new Point(13, 3);
            bitmapPoints[4, 6] = new Point(2, 4);
            bitmapPoints[4, 7] = new Point(7, 10);
            bitmapPoints[4, 8] = new Point(11, 11);
            bitmapPoints[4, 9] = new Point(12, 6);



            bitmapPoints[5, 0] = new Point(11, 1);
            bitmapPoints[5, 1] = new Point(1, 1);
            bitmapPoints[5, 2] = new Point(1, 14);
            bitmapPoints[5, 3] = new Point(11, 8);
            bitmapPoints[5, 4] = new Point(12, 13);

            bitmapPoints[5, 5] = new Point(6, 17);
            bitmapPoints[5, 6] = new Point(6, 1);
            bitmapPoints[5, 7] = new Point(1, 8);
            bitmapPoints[5, 8] = new Point(2, 4);
            bitmapPoints[5, 9] = new Point(1, 2);



            bitmapPoints[6, 0] = new Point(1, 12);
            bitmapPoints[6, 1] = new Point(10, 1);
            bitmapPoints[6, 2] = new Point(4, 1);
            bitmapPoints[6, 3] = new Point(12, 14);
            bitmapPoints[6, 4] = new Point(1, 4);

            bitmapPoints[6, 5] = new Point(5, 10);
            bitmapPoints[6, 6] = new Point(2, 10);
            bitmapPoints[6, 7] = new Point(2, 1);
            bitmapPoints[6, 8] = new Point(-12, -5);
            bitmapPoints[6, 9] = new Point(12, 17);



            bitmapPoints[7, 0] = new Point(1, 1);
            bitmapPoints[7, 1] = new Point(1, 18);
            bitmapPoints[7, 2] = new Point(3, 13);
            bitmapPoints[7, 3] = new Point(7, 9);
            bitmapPoints[7, 4] = new Point(12, 3);

            bitmapPoints[7, 5] = new Point(5, 1);
            bitmapPoints[7, 6] = new Point(10, 1);
            bitmapPoints[7, 7] = new Point(2, 17);
            bitmapPoints[7, 8] = new Point(7, 8);
            bitmapPoints[7, 9] = new Point(11, 6);



            bitmapPoints[8, 0] = new Point(5, 10);
            bitmapPoints[8, 1] = new Point(2, 11);
            bitmapPoints[8, 2] = new Point(11, 11);
            bitmapPoints[8, 3] = new Point(2, 2);
            bitmapPoints[8, 4] = new Point(11, 1);

            bitmapPoints[8, 5] = new Point(12, 9);
            bitmapPoints[8, 6] = new Point(5, 17);
            bitmapPoints[8, 7] = new Point(7, 1);
            bitmapPoints[8, 8] = new Point(12, 5);
            bitmapPoints[8, 9] = new Point(1, 5);



            bitmapPoints[9, 0] = new Point(13, 9);
            bitmapPoints[9, 1] = new Point(7, 1);
            bitmapPoints[9, 2] = new Point(1, 5);
            bitmapPoints[9, 3] = new Point(1, 14);
            bitmapPoints[9, 4] = new Point(11, 17);

            bitmapPoints[9, 5] = new Point(2, 15);
            bitmapPoints[9, 6] = new Point(6, 18);
            bitmapPoints[9, 7] = new Point(12, 15);
            bitmapPoints[9, 8] = new Point(3, 9);
            bitmapPoints[9, 9] = new Point(10, 2);




            var cc = new ColorConverter();
            activeColors.Add("#b20137", "special");
            activeColors.Add("#5a6e28", "energy");
            activeColors.Add("#4b408f", "sniper");
            activeColors.Add("#386b59", "heavy");
            activeColors.Add("#386a59", "heavy");
            activeColors.Add("#7d542d", "light");
            activeColors.Add("#7c542d", "light");
            activeColors.Add("#6b2007", "shotgun");

            inactiveColors.Add("#54121f", "special");
            inactiveColors.Add("#2e371d", "energy");
            inactiveColors.Add("#241f43", "sniper");
            inactiveColors.Add("#21342d", "heavy");
            inactiveColors.Add("#3c2a1c", "light");
            inactiveColors.Add("#30110a", "shotgun");



            weaponNames.Add("cafe er anien", "EVA-8 AUTO");
            weaponNames.Add("ai) ei ien", "EVA-8 AUTO");
            weaponNames.Add("ae eansen", "EVA-8 AUTO");

            weaponNames.Add("Silla", "MASTIFF");
            weaponNames.Add("Sls", "MASTIFF");
            weaponNames.Add("erdsy\\ziise", "KRABER");
            weaponNames.Add("erdsy\\z ise", "KRABER");
            weaponNames.Add("ia) ayenniel", "DEVOTION");
            weaponNames.Add("rey Wolo", "HAVOC");




            weaponNames.Add("ess", "L-STAR");
            weaponNames.Add("is)|2)e sare", "TRIPLE TAKE");
            weaponNames.Add("foey\\icj-8\") | e", "CHARGE RIFLE");
            weaponNames.Add("aCe", "SENTINEL");
            weaponNames.Add("CHARGE RIFLE.", "CHARGE RIFLE");
            weaponNames.Add("aaa", "SENTINEL");
            weaponNames.Add("a0 veel", "FLATLINE");
            weaponNames.Add("ae Viel", "FLATLINE");
            weaponNames.Add("ae viel", "FLATLINE");
            weaponNames.Add("a0 Vee", "FLATLINE");

            weaponNames.Add("his)", "SPITFIRE");
            weaponNames.Add("ose) haaie] =", "SPITFIRE");
            weaponNames.Add("aN", "WINGMAN");
            weaponNames.Add("cis) heals]", "SPITFIRE");
            weaponNames.Add("aN ee", "WINGMAN");
            weaponNames.Add("cis) heal]", "SPITFIRE");
            weaponNames.Add("Veal) vaelse", "ALTERNATOR");
            weaponNames.Add("coche)", "R-99");
            weaponNames.Add("c= mcie)", "R-99");
            weaponNames.Add("[2]", "RE-45");
            weaponNames.Add("P2020", "P2020");

            weaponNames.Add("G7SCOUT", "G7 SCOUT");
            weaponNames.Add("ELECTROGEWEHF", "CHARGE RIFLE");
            weaponNames.Add("ALTERNATOR", "ALTERNATOR");
            weaponNames.Add("RE-45", "RE-45");
            weaponNames.Add("R-301", "R-301");
            weaponNames.Add("R-99", "R-99");

            weaponNames.Add("WINGMAN", "WINGMAN");
            weaponNames.Add("SPITFIRE", "SPITFIRE");
            weaponNames.Add("FLATLINE", "FLATLINE");
            weaponNames.Add("cae viel", "FLATLINE");
            weaponNames.Add("cae Viel", "FLATLINE");
            weaponNames.Add("[ans", "PEACEKEEPER");
            weaponNames.Add("MOSAMBIK", "MOZAMBIQUE");
            weaponNames.Add("ae ey anian", "EVA-8 AUTO");

            weaponNames.Add("io) ayanniel", "DEVOTION");

            weaponNames.Add("es)|:)e sae", "TRIPLE TAKE");
            weaponNames.Add("f3 *))a)s\\ oye", "PEACEKEEPER");
            weaponNames.Add("a / eceanian", "EVA-8 AUTO");

            weaponNames.Add("HEMLOK", "HEMLOK");
            weaponNames.Add("CHARGERIFLE", "CHARGERIFLE");
            weaponNames.Add("SENTINEL", "SENTINEL");
            weaponNames.Add("TRIPLE TAKE", "TRIPLE TAKE");
            weaponNames.Add("LONGBOW", "LONGBOW");
            weaponNames.Add("DEVOTION", "DEVOTION");
            weaponNames.Add("VOLT", "VOLT");
            weaponNames.Add("HAVOC", "HAVOC");
            weaponNames.Add("L-STAR", "L-STAR");
            weaponNames.Add("MASTIFF", "MASTIFF");
            weaponNames.Add("MOZAMBIQUE", "MOZAMBIQUE");
            weaponNames.Add("EVA-8 AUTO", "EVA-8 AUTO");
            weaponNames.Add("EVA-8AUTO", "EVA-8 AUTO");
            weaponNames.Add("PROWLER", "PROWLER");
            weaponNames.Add("CHARGE RIFLE", "CHARGE RIFLE");
            weaponNames.Add("KRABER", "KRABER");
            weaponNames.Add("PEACEKEEPER", "PEACEKEEPER");
            weaponNames.Add("", "???");


            weapons.Add("ALTERNATOR", new Weapon("Alternator", "light"));
            weapons.Add("G7 SCOUT", new Weapon("G7 scout", "light"));
            weapons.Add("R-301", new Weapon("R-301", "light"));
            weapons.Add("R-99", new Weapon("R-99", "light"));
            weapons.Add("P2020", new Weapon("P2020", "light"));
            weapons.Add("RE-45", new Weapon("RE-45", "light"));
            weapons.Add("FLATLINE", new Weapon("Flatline", "heavy"));
            weapons.Add("HEMLOK", new Weapon("Hemlok", "heavy"));
            weapons.Add("WINGMAN", new Weapon("Wingman", "heavy"));
            weapons.Add("SPITFIRE", new Weapon("Spitfire", "heavy"));
            weapons.Add("CHARGE RIFLE", new Weapon("Charge Rifle", "sniper"));
            weapons.Add("SENTINEL", new Weapon("Sentinel", "sniper"));
            weapons.Add("LONGBOW", new Weapon("Longbow", "sniper"));
            weapons.Add("TRIPLE TAKE", new Weapon("Triple Take", "sniper"));
            weapons.Add("VOLT", new Weapon("Volt", "energy"));
            weapons.Add("DEVOTION", new Weapon("Devotion", "energy"));
            weapons.Add("HAVOC", new Weapon("Havoc", "energy"));
            weapons.Add("L-STAR", new Weapon("L-Star", "energy"));
            weapons.Add("KRABER", new Weapon("Kraber", "special"));
            weapons.Add("PROWLER", new Weapon("Prowler", "special"));
            weapons.Add("PEACEKEEPER", new Weapon("Peacekeeper", "special"));
            weapons.Add("MOZAMBIQUE", new Weapon("Mozambique", "shotgun"));
            weapons.Add("EVA-8 AUTO", new Weapon("EVA-8 auto", "shotgun"));
            weapons.Add("MASTIFF", new Weapon("Mastiff", "shotgun"));
            weapons.Add("???", new Weapon("???", "???"));
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private static IKeyboardMouseEvents m_GlobalHook;
        public void SubscribeKeyHook()
        {
            m_GlobalHook = Hook.GlobalEvents();
            m_GlobalHook.KeyPress += GlobalHookKeyPress;
        }
        private void GlobalHookKeyPress(object sender, KeyPressEventArgs e)
        {
            Console.WriteLine("KeyPress >" + e.KeyChar + "<");

            if (e.KeyChar == userData.inventoryKey)
            {
                Console.WriteLine("Tab pressed!");

            }
            if (e.KeyChar == userData.debugModeKey)
            {
                Console.WriteLine("Toggle DebugMode");
                toggleDebugMode();
            }
            if (e.KeyChar == userData.switchPlayerPosKey)
            {
                Console.WriteLine("Switch Player render pos");
                var x = userData.player1Name;
                userData.player1Name = userData.player2Name;
                userData.player2Name = x;
                floatingOsdWindow.switchedPos = !floatingOsdWindow.switchedPos;

                renderUserInformation();
                floatingOsdWindow.Invalidate();
            }
            if (e.KeyChar == userData.boxMoveModeKey)
            {
                floatingOsdWindow.boxMoveMode = !floatingOsdWindow.boxMoveMode;
                floatingOsdWindow.Invalidate();
            }
            if (floatingOsdWindow.boxMoveMode)
            {
                if (e.KeyChar == 'j')//Left
                {
                    floatingOsdWindow.moveBox(-1, 0, 0, 0);
                    floatingOsdWindow.Invalidate();
                }
                if (e.KeyChar == 'i')//Up
                {
                    floatingOsdWindow.moveBox(0, -1, 0, 0);
                    floatingOsdWindow.Invalidate();

                }
                if (e.KeyChar == 'l')//Right
                {
                    floatingOsdWindow.moveBox(1, 0, 0, 0);
                    floatingOsdWindow.Invalidate();
                }
                if (e.KeyChar == 'k')//Down
                {
                    floatingOsdWindow.moveBox(0, 1, 0, 0);
                    floatingOsdWindow.Invalidate();
                }
                if (e.KeyChar == userData.boxIncreaseHeightKey)//Down
                {
                    floatingOsdWindow.moveBox(0, 0, 0, 1);
                    floatingOsdWindow.Invalidate();
                }
            }
        }
        public void Unsubscribe()
        {
            m_GlobalHook.KeyPress -= GlobalHookKeyPress;
            m_GlobalHook.Dispose();
        }
    }
}
