﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    [Serializable]
    class User
    {
        public string name;
        public string weapon1;
        public string weapon1ammo;
        public string weapon2;
        public string weapon2ammo;
        public string weapon1type;
        public string weapon2type;
        public string ammoImg1;
        public string ammoImg2;
        public string ammoImg1Color;
        public string ammoImg2Color;
        public bool loopBack;
        public User(string name,string weapon1,string weapon1ammo,string weapon2, string weapon2ammo, string weapon1Type, string weapon2Type) {
            this.name = name;
            this.weapon1 = weapon1;
            this.weapon1ammo = weapon1ammo;
            this.weapon2 = weapon2;
            this.weapon2ammo = weapon2ammo;
            this.weapon1type = weapon1Type;
            this.weapon2type = weapon2Type;
        }
    }
    public class Location {
        public int x;
        public int y;
    }
    public class Box {
        public int x;
        public int y;
        public int width;
        public int height;
    }
    class UserData
    {
        public string name;
        public char inventoryKey;
        public char debugModeKey;
        public char boxMoveModeKey;
        public char boxIncreaseWidthKey;
        public char boxDecreaseWidthKey;
        public char boxIncreaseHeightKey;
        public char boxDecreaseHeightKey;
        public char switchPlayerPosKey;
        public string player1Name;
        public string player2Name;
        public bool showOwnStats;
        public bool debugMode;
        public bool checkNumbers;
        public bool loopBack;
        public int ammoSpacing;
        public Box screenBox;
        public Box player1NameBox;
        public Box player2NameBox;
        public Box weapon1ReadBox;
        public Box weapon2ReadBox;
        public Box ammoReadBox;
        public Location player1Weapon1DrawLocation;
        public Location player1Weapon2DrawLocation;
        public Location player2Weapon1DrawLocation;
        public Location player2Weapon2DrawLocation;
        public Location player1Ammo1DrawLocation;
        public Location player1Ammo2DrawLocation;
        public Location player2Ammo1DrawLocation;
        public Location player2Ammo2DrawLocation;

      
    }
}
