﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.player1Ammo2 = new System.Windows.Forms.Label();
            this.player1Ammo1 = new System.Windows.Forms.Label();
            this.keysLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // player1Ammo2
            // 
            this.player1Ammo2.AutoSize = true;
            this.player1Ammo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.player1Ammo2.ForeColor = System.Drawing.Color.White;
            this.player1Ammo2.Location = new System.Drawing.Point(12, 57);
            this.player1Ammo2.Name = "player1Ammo2";
            this.player1Ammo2.Size = new System.Drawing.Size(40, 24);
            this.player1Ammo2.TabIndex = 8;
            this.player1Ammo2.Text = "???";
            // 
            // player1Ammo1
            // 
            this.player1Ammo1.AutoSize = true;
            this.player1Ammo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.player1Ammo1.ForeColor = System.Drawing.Color.White;
            this.player1Ammo1.Location = new System.Drawing.Point(13, 33);
            this.player1Ammo1.Name = "player1Ammo1";
            this.player1Ammo1.Size = new System.Drawing.Size(40, 24);
            this.player1Ammo1.TabIndex = 9;
            this.player1Ammo1.Text = "???";
            // 
            // keysLabel
            // 
            this.keysLabel.AutoSize = true;
            this.keysLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.keysLabel.ForeColor = System.Drawing.Color.White;
            this.keysLabel.Location = new System.Drawing.Point(13, 81);
            this.keysLabel.Name = "keysLabel";
            this.keysLabel.Size = new System.Drawing.Size(56, 24);
            this.keysLabel.TabIndex = 10;
            this.keysLabel.Text = "Keys:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.keysLabel);
            this.Controls.Add(this.player1Ammo1);
            this.Controls.Add(this.player1Ammo2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label player1Ammo2;
        private System.Windows.Forms.Label player1Ammo1;
        private System.Windows.Forms.Label keysLabel;
    }
}

