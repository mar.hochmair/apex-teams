Prequisites:
-Borderless window statt fullscreen
-1920x1080 als auflösung
-"Button hints" auf "aus" (Settings=>Gameplay=>2. option)

Anleitung:
1) userData.json öffnen
2) "name" auf eigenen namen umschreiben.
3) "player1Name" auf "name" eines anderen mitspielers umschreiben
4) "player2Name" auf "name" des 2. anderen mitspielers umschreiben
5) "inventoryKey" auf den key, der das inventar öffnet umschreiben. "\t" ist TAB. beispiele: a,d,i(kleingeschrieben!)