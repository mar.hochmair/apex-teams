﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class WeaponDraw
    {
        public Bitmap weaponBg;
        public string weaponName;
        public int posX;
        public int posY;
        public WeaponDraw()
        {
        }
        public WeaponDraw(Bitmap weaponBg, string weaponName, int posX, int posY)
        {
            this.weaponBg = weaponBg;
            this.weaponName = weaponName;
            this.posX = posX;
            this.posY = posY;
        }
    }
    public class AmmoDraw
    {
        public Bitmap ammoBg;
        public string count;
        public int posX;
        public int posY;
        public AmmoDraw()
        {
        }

    }
    public class FloatingOSDWindow : FloatingWindow
    {
        private List<Rectangle> boxes;
        private List<String> boxNames;
        public WeaponDraw player1weapon1Draw = new WeaponDraw();
        public WeaponDraw player1weapon2Draw = new WeaponDraw();
        public WeaponDraw player2weapon1Draw = new WeaponDraw();
        public WeaponDraw player2weapon2Draw = new WeaponDraw();
        public AmmoDraw player1ammo1Draw = new AmmoDraw();
        public AmmoDraw player1ammo2Draw = new AmmoDraw();
        public AmmoDraw player2ammo1Draw = new AmmoDraw();
        public AmmoDraw player2ammo2Draw = new AmmoDraw();

        public bool player1NameShow = false;
        public bool player2NameShow = false;
        public string player1Name = "";
        public string player2Name = "";
        public bool switchedPos = false;
        public bool debugMode = false;
        public string Weapon1ReadText = "";
        public string Weapon2ReadText = "";
        public bool Weapon1NameInDic = false;
        public bool Weapon2NameInDic = false;
        public string ammo1ReadText = "";
        public string ammo2ReadText = "";
        public string ammo3ReadText = "";
        public int activeBoxIndex = 0;
        public bool boxMoveMode = false;
        Font drawFont = new Font("Arial", 8);
        Font drawFontBig = new Font("Arial", 14);
        SolidBrush drawBrush = new SolidBrush(Color.White);

        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        public FloatingOSDWindow(int x, int y, int width, int height, bool debugMode)
        {
            base.Location = new Point(x, y);
            boxes = new List<Rectangle>();
            boxNames = new List<String>();
            base.Size = new Size(width, height);
            this.debugMode = debugMode;
            myTimer.Tick += new EventHandler(TimerEventProcessor);
            base.Show();
            // Sets the timer interval to 5 seconds.
            myTimer.Interval = 1000;
            // myTimer.Start();

        }

        public void TimerEventProcessor(Object myObject,
                                           EventArgs myEventArgs)
        {
            base.Invalidate();


        }
        public void CreateBox(int x, int y, int width, int height, string name)
        {
            boxes.Add(new Rectangle(x - 1, y - 1, width + 2, height + 2));
            boxNames.Add(name);
        }

        #region Overrided Drawing & Path Creation
        protected override void PerformPaint(PaintEventArgs e)
        {
            if (base.Handle == IntPtr.Zero)
                return;
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.HighQuality;
            var pen = new Pen(Color.White);
            if (debugMode)
            {
                boxes.ForEach(box =>
                {
                    var name = boxNames[boxes.IndexOf(box)];
                    if (name == "weapon1ReadBox")
                    {
                        drawBrush.Color = Weapon1NameInDic ? Color.Green : Color.Red;
                        g.DrawString("\"" + Weapon1ReadText + "\"", drawFontBig, drawBrush, box.X, box.Y + 20);
                        drawBrush.Color = Color.White;
                    }
                    if (name == "weapon2ReadBox")
                    {
                        drawBrush.Color = Weapon2NameInDic ? Color.Green : Color.Red;
                        g.DrawString("\"" + Weapon2ReadText + "\"", drawFontBig, drawBrush, box.X, box.Y + 20);
                        drawBrush.Color = Color.White;
                    }
                    if (name == "ammoReadBox1")
                    {
                        g.DrawString("\"" + ammo1ReadText + "\"", drawFontBig, drawBrush, box.X - 30, box.Y - 90);
                    }
                    if (name == "ammoReadBox2")
                    {
                        g.DrawString("\"" + ammo2ReadText + "\"", drawFontBig, drawBrush, box.X, box.Y - 90);
                    }
                    if (name == "ammoReadBox3")
                    {
                        g.DrawString("\"" + ammo3ReadText + "\"", drawFontBig, drawBrush, box.X + 30, box.Y - 90);
                    }
                    if (activeBoxIndex == boxes.IndexOf(box) && boxMoveMode)
                    {
                        pen.Color = Color.Green;
                    }
                    g.DrawRectangle(pen, new Rectangle(box.X, box.Y, box.Width, box.Height));
                    pen.Color = Color.White;
                });
                drawPlayerName(player1ammo1Draw, g, 1);
                drawPlayerName(player2ammo1Draw, g, 2);
            }
            drawWeapon(player1weapon1Draw, g);
            drawWeapon(player1weapon2Draw, g);
            drawWeapon(player2weapon1Draw, g);
            drawWeapon(player2weapon2Draw, g);
            drawAmmo(player1ammo1Draw, g);
            drawAmmo(player1ammo2Draw, g);
            drawAmmo(player2ammo1Draw, g);
            drawAmmo(player2ammo2Draw, g);
        }
        public void resetPlayerRender()
        {
            player1weapon1Draw.weaponBg = null;
            player1weapon2Draw.weaponBg = null;
            player2weapon1Draw.weaponBg = null;
            player2weapon2Draw.weaponBg = null;
            player1ammo1Draw.ammoBg = null;
            player1ammo2Draw.ammoBg = null;
            player2ammo1Draw.ammoBg = null;
            player2ammo2Draw.ammoBg = null;
        }
        public void moveBox(int shiftX,int shiftY,int shiftWidth,int shiftHeight)
        {
            var b = boxes[activeBoxIndex];
            b.X += shiftX;
            b.Y += shiftY;
            b.Width += shiftWidth;
            b.Height += shiftHeight;
            boxes[activeBoxIndex] = b;
        }
        #endregion 
        private void drawWeapon(WeaponDraw w, Graphics g)
        {

            if (w.weaponBg != null)
            {
                g.DrawImage(w.weaponBg, w.posX, w.posY);
                var strWidth = g.MeasureString(w.weaponName, drawFont).Width;
                g.DrawString(w.weaponName, drawFont, drawBrush, w.posX + 50 - (strWidth / 2), w.posY);
            }
        }
        private void drawPlayerName(AmmoDraw w, Graphics g, int player)
        {
            if (player == 1 && !switchedPos || player == 2 && switchedPos) g.DrawString(player1Name, drawFont, drawBrush, w.posX, w.posY - 40);
            if (player == 2 && !switchedPos || player == 1 && switchedPos) g.DrawString(player2Name, drawFont, drawBrush, w.posX, w.posY - 40);
        }
        private void drawAmmo(AmmoDraw w, Graphics g)
        {

            if (w.ammoBg != null)
            {
                g.DrawImage(w.ammoBg, w.posX, w.posY);
                var strWidth = g.MeasureString(w.count, drawFont).Width;
                g.DrawString(w.count, drawFont, drawBrush, w.posX + 30 - (strWidth / 2), w.posY);
            }
        }
    }
}