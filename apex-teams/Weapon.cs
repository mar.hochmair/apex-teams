﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace WindowsFormsApp1
{
    class Weapon
    {
        public string ammoType;
        public string name;
        public Weapon(string name, string ammoType)
        {
            this.name = name;
            this.ammoType = ammoType;
        }
    }
}
