﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using Gma.System.MouseKeyHook;

namespace WindowsFormsApp1
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 f = new Form1();
            f.StartPosition = FormStartPosition.Manual;
            f.BackColor = Color.LightGray;
            f.TransparencyKey = Color.LightGray;
            f.FormBorderStyle = FormBorderStyle.None;
            f.TopMost = true;

            f.Bounds = new Rectangle(0, 400, 2000, 2000);
            Application.Run(f);
        }


    }
}

